# B1 Réseau 2021 - TP1

# TP1 - Mise en jambes


# I. Exploration locale en solo

## 1. Affichage d'informations sur la pile TCP/IP locale

### En ligne de commande

  On affiche les infos des cartes réseaux ainsi que leur gateways à l'aide de ```ipconfig```
  
  ```
    C:\Users\33785>ipconfig

Configuration IP de Windows


    
    <...>
    
    Carte réseau sans fil Wi-Fi :

   Suffixe DNS propre à la connexion. . . : auvence.co
   Adresse IPv6 de liaison locale. . . . .: fe80::7c10:c269:b41b:79f1%20
   Adresse IPv4. . . . . . . . . . . . . .: 10.33.2.87
   Masque de sous-réseau. . . . . . . . . : 255.255.252.0
   Passerelle par défaut. . . . . . . . . : 10.33.3.253
   ```
   
   Via l'interface graphique, on peut trouver ces informations dans "Afficher les propriétés du matériel et de la connexion"
   
   ![](./I.1.png)
   
   La gateway du réseau Ynov permet de rediriger les requêtes envoyées depuis le LAN d'Ynov vers l'extérieur.
   
   
   ## 2. Modifications des informations

### A. Modification d'adresse IP (part 1) 

   Pour modifier l'adresse ip en interface graphique, on le fait depuis les propriétés de la connexion établie.

![](./IIA.png)


On peut perdre l'accés internet si on attribue une adresse IP qui a déjà été attribuée par le serveur DHCP.
    
    
    
    
### B. Table ARP

    Affichage de la table ARP:
    
    ```
    C:\Users\33785>arp -a

    Interface : 10.10.10.2 --- 0x10
      Adresse Internet      Adresse physique      Type
      10.10.10.255          ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique

    Interface : 10.33.2.87 --- 0x14
      Adresse Internet      Adresse physique      Type
      10.33.0.24            d8-12-65-b5-11-77     dynamique
      10.33.1.67            58-96-1d-14-35-6b     dynamique
      10.33.2.16            70-66-55-8b-df-07     dynamique
      10.33.2.93            d8-12-65-b5-11-77     dynamique
      10.33.2.97            d8-12-65-b5-11-77     dynamique
      10.33.2.209           5c-87-9c-e4-44-2c     dynamique
      10.33.3.253           00-12-00-40-4c-bf     dynamique
      10.33.3.254           00-0e-c4-cd-74-f5     dynamique
      10.33.3.255           ff-ff-ff-ff-ff-ff     statique
      224.0.0.22            01-00-5e-00-00-16     statique
      224.0.0.251           01-00-5e-00-00-fb     statique
      224.0.0.252           01-00-5e-00-00-fc     statique
      239.255.255.250       01-00-5e-7f-ff-fa     statique
      255.255.255.255       ff-ff-ff-ff-ff-ff     statique
      <...>
      ``
   Je pense que l'adresse MAC est ff-ff-ff-ff-ff-ff car c'est la seule dont l'adresse ip est statique.
   
   Après avoir ping les adresses ip écrites aau tableau j'obtiens la table suivante:
   
   ``
   Interface : 10.33.2.87 --- 0x14
  Adresse Internet      Adresse physique      Type
  10.33.0.24            d8-12-65-b5-11-77     dynamique
  10.33.0.115           ac-67-5d-83-92-62     dynamique
  10.33.0.208           70-66-55-c5-4e-29     dynamique
  10.33.1.8             0c-dd-24-aa-e1-87     dynamique
  10.33.1.67            58-96-1d-14-35-6b     dynamique
  10.33.2.16            70-66-55-8b-df-07     dynamique
  10.33.2.93            d8-12-65-b5-11-77     dynamique
  10.33.2.97            d8-12-65-b5-11-77     dynamique
  10.33.2.105           ec-2e-98-ca-da-e9     dynamique
  10.33.2.209           5c-87-9c-e4-44-2c     dynamique
  10.33.3.28            e4-5e-37-3a-65-fb     dynamique
  10.33.3.109           38-f9-d3-2f-c2-79     dynamique
  10.33.3.112           3c-06-30-2d-48-0d     dynamique
  10.33.3.253           00-12-00-40-4c-bf     dynamique
  10.33.3.254           00-0e-c4-cd-74-f5     dynamique
  10.33.3.255           ff-ff-ff-ff-ff-ff     statique
  224.0.0.22            01-00-5e-00-00-16     statique
  224.0.0.251           01-00-5e-00-00-fb     statique
  224.0.0.252           01-00-5e-00-00-fc     statique
  239.255.255.250       01-00-5e-7f-ff-fa     statique
  255.255.255.255       ff-ff-ff-ff-ff-ff     statique
  ``
  et voici les nouvelles adresses MAC récupérées: 
  
  38-f9-d3-2f-c2-79 
  3c-06-30-2d-48-0d
  
  ### C. `nmap`
  
  scan du réseau via nmap:
 
  ``
      C:\Users\33785>nmap -sP 10.33.0.0/22
    Starting Nmap 7.92 ( https://nmap.org ) at 2021-09-13 17:42 Paris, Madrid (heure dÆÚtÚ)
    Nmap scan report for 10.33.0.10
    Host is up (1.1s latency).
    MAC Address: B0:6F:E0:4C:CF:EA (Samsung Electronics)
    Nmap scan report for 10.33.0.20
    Host is up (0.0080s latency).
    MAC Address: 74:29:AF:33:1B:69 (Hon Hai Precision Ind.)
    Nmap scan report for 10.33.0.21
    Host is up (0.058s latency).
    MAC Address: B0:FC:36:CE:9C:89 (CyberTAN Technology)
    <...>
    ``
    
   la nouvelle table ARP:


 ```
C:\Users\33785>arp -a

<...>

Interface : 10.33.2.87 --- 0x14
  Adresse Internet      Adresse physique      Type
  10.33.0.3             22-93-d2-7e-9f-1a     dynamique
  10.33.0.20            74-29-af-33-1b-69     dynamique
  10.33.0.24            d8-12-65-b5-11-77     dynamique
  10.33.0.45            f2-4a-90-92-79-12     dynamique
  10.33.0.59            e4-0e-ee-73-73-96     dynamique
  10.33.0.115           ac-67-5d-83-92-62     dynamique
  10.33.0.155           82-96-88-2f-06-e7     dynamique
<...>
```


On peut donc voir que '10.33.0.36' est libre:

Après modification voici la nouvelle table ARP :

```
    C:\Users\33785>nmap -sP 10.33.0.36/22
    Interface : 10.33.2.236 --- 0x6
      Adresse Internet      Adresse physique      Type
      10.33.0.3             22-93-d2-7e-9f-1a     dynamique
      10.33.0.10            b0-6f-e0-4c-cf-ea     dynamique
      10.33.0.31            fa-c5-6d-60-4b-4c     dynamique
      10.33.0.41            b0-eb-57-82-54-35     dynamique
      10.33.0.71            f0-03-8c-35-fe-47     dynamique
      10.33.0.124           36-98-05-f5-ee-5a     dynamique
      10.33.0.164           9a-75-ee-ef-e2-00     dynamique
      10.33.0.174           40-ec-99-c6-dc-c5     dynamique
      10.33.0.175           74-d8-3e-f2-cc-95     dynamique
      10.33.0.234           c8-58-c0-27-22-fc     dynamique
      10.33.0.243           dc-21-5c-3b-a3-f3     dynamique
      10.33.1.48            34-42-62-23-f7-3c     dynamique
      10.33.1.69            a0-a4-c5-41-bf-4f     dynamique
      10.33.1.101           e0-cc-f8-7d-b8-fc     dynamique
      10.33.1.112           9c-fc-e8-b8-0e-02     dynamique
      10.33.1.118           a2-24-4b-f2-bf-6a     dynamique
      10.33.1.146           0c-54-15-44-1a-56     dynamique
      10.33.1.148           4a-c8-d7-aa-c1-d4     dynamique
      10.33.1.172           50-e0-85-db-61-8b     dynamique
      10.33.1.182           5e-1d-0e-63-78-34     dynamique
```

# II. Exploration locale en duo

   ## 3. Modification d'adresse IP


vérification de la prise en compte des changements 


    ```
    C:\Users\33785>ipconfig

    Configuration IP de Windows


    Carte Ethernet Ethernet :

    Suffixe DNS propre à la connexion. . . :
    Adresse IPv6 de liaison locale. . . . .: fe80::4dc6:7c9:5de4:ab84%9
    Adresse IPv4. . . . . . . . . . . . . .: 192.168.0.2
    Masque de sous-réseau. . . . . . . . . : 255.255.255.252
    Passerelle par défaut. . . . . . . . . : 192.168.0.3
    ```

ping vers le second pc:
    
    ```
    C:\Users\33785>ping 192.168.0.1

    Envoi d’une requête 'Ping'  192.168.0.1 avec 32 octets de données :
    Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=128
    Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=128
    Réponse de 192.168.0.1 : octets=32 temps=3 ms TTL=128
    Réponse de 192.168.0.1 : octets=32 temps=1 ms TTL=128

    Statistiques Ping pour 192.168.0.1:
    Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
    Minimum = 1ms, Maximum = 3ms, Moyenne = 1ms
    ```
    
   Affichage de la table ARP:

```
   C:\Users\33785>arp -a

Interface : 192.168.0.2 --- 0x9
  Adresse Internet      Adresse physique      Type
  192.168.0.1           2c-f0-5d-d3-dc-93     dynamique
  192.168.0.3           ff-ff-ff-ff-ff-ff     statique
```

   ## 4. Utilisation d'un des deux comme gateway

`Ping` vers 8.8.8.8 : 

```
    C:\Users\Fabio>ping 8.8.8.8

    Envoi d'une requête 'Ping'  8.8.8.8 avec 32 octets de données :
    Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=115
    Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=115
    Réponse de 8.8.8.8 : octets=32 temps=22 ms TTL=115
    Réponse de 8.8.8.8 : octets=32 temps=21 ms TTL=115

    Statistiques Ping pour 8.8.8.8:
        Paquets : envoyés = 4, reçus = 4, perdus = 0 (perte 0%),
    Durée approximative des boucles en millisecondes :
        Minimum = 21ms, Maximum = 22ms, Moyenne = 21ms
```


 Utilisation de `tracert` pour vérifier que les requêtes passent bien par l'autre pc portable : 
 
```
     C:\Users\Fabio>tracert 8.8.8.8

    Détermination de l'itinéraire vers dns.google [8.8.8.8]
    avec un maximum de 30 sauts :

      1     1 ms     *        1 ms  LAPTOP-6AO987H5.mshome.net [192.168.137.1]
      <...>
```


  ## 5. Petit chat privé
  
  
  à l'aide de `netcat` on obtient le résultat suivant: 
  
  coté serveur :
```
  C:\Users\33785\Desktop\netcat>nc.exe -l -p 4242
bijourr

^C
```

 coté client : 

```
 
 C:\Users\Fabio\Desktop\netcat>nc.exe 192.168.137.1 4242
bijourr

^C
```

et lorsque l'on précise sur quelle IP écouter :
 "mauvaise IP" :
```
    C:\Users\33785\Desktop\netcat>nc.exe -l -p 4242 192.168.123.0
invalid connection to [192.168.137.1] from (UNKNOWN) [192.168.137.98] 65033
```
 "bonne IP"
```
    C:\Users\33785\Desktop\netcat>nc.exe -l -p 4242 192.168.137.98
    gf
```


## 6. Firewall

   Pour autoriser les ping vers mon pc je créés 2 règles dans mon firewall via `netsh` : 
   
```
   PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
Ok.

PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V6 echo request" protocol=icmpv6:8,any dir=in action=allow
Ok.
```

 On peut ainsi voir que les ping passent :
 ```
PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V4 echo request" protocol=icmpv4:8,any dir=in action=allow
Ok.

PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="ICMP Allow incoming V6 echo request" protocol=icmpv6:8,any dir=in action=allow
Ok.
```

 Création d'une règle pour autoriser `netcat` via `netsh` :
```
     PS C:\WINDOWS\system32> netsh advfirewall firewall add rule name="Allow TCP on port 4242" dir=in action=allow protocol=TCP localport=4242
    Ok.
```

On peut donc de nouveau utiliser `netcat` :
```
     C:\Users\33785\Desktop\netcat>nc.exe -l -p 4242
    grfgg
    yhed
    dfgh*
```
 
 # III. Manipulations d'autres outils/protocoles côté client

## 1. DHCP

en faisant `ipconfig /all` on obtient la durée du bail DHCP:


```
    C:\Users\33785\Desktop\netcat>ipconfig /all
    Carte réseau sans fil Wi-Fi :

       <...>
       Bail obtenu. . . . . . . . . . . . . . : jeudi 16 septembre 2021 13:33:38
       Bail expirant. . . . . . . . . . . . . : jeudi 16 septembre 2021 16:33:29
       <...>
```


## 2. DNS

en faisant `ipconfig /all` on obtient l'adresse IP du serveur DNS:

```
    C:\Users\33785\Desktop\netcat>ipconfig /all
    Carte réseau sans fil Wi-Fi :
      <...>
       Serveurs DNS. . .  . . . . . . . . . . : 10.33.10.2
                                           10.33.10.148
                                           10.33.10.155
      <...>

```

`nslookup` pour google.com et ynov.com :

```
    C:\Users\33785\Desktop\netcat>nslookup google.com
    Serveur :   UnKnown
    Address:  10.33.10.2

    Réponse ne faisant pas autorité :
    Nom :    google.com
    Addresses:  2a00:1450:4007:813::200e
              216.58.206.238


    C:\Users\33785\Desktop\netcat>nslookup ynov.com
    Serveur :   UnKnown
    Address:  10.33.10.2

    Réponse ne faisant pas autorité :
    Nom :    ynov.com
    Address:  92.243.16.143
```

J'en déduis que le DNS sert à mettre en relation un nom de domaine et l'adresse IP à laquelle il est associé.

On a envoyé ces requêtes au serveur DNS d'Ynov, on donc déjà son adresse IP qui est 10.33.0.2.


"Reverse lookup"

```
    C:\Users\33785\Desktop\netcat>reverse lookup 78.74.21.21
    'reverse' n’est pas reconnu en tant que commande interne
    ou externe, un programme exécutable ou un fichier de commandes.

    C:\Users\33785\Desktop\netcat>nslookup 78.74.21.21
    Serveur :   UnKnown
    Address:  10.33.10.2

    Nom :    host-78-74-21-21.homerun.telia.com
    Address:  78.74.21.21


    C:\Users\33785\Desktop\netcat>nslookup 92.146.54.88
    Serveur :   UnKnown
    Address:  10.33.10.2

    Nom :    apoitiers-654-1-167-88.w92-146.abo.wanadoo.fr
    Address:  92.146.54.88
```
On peut donc voir que le serveur DNS a pu récupérer des noms de domaines à partir des adresses IP.

# IV. Wireshark


Screen du `ping`:

![](./IVPing.png)


screen du `netcat`:

![](./IVNetcat.png)


Screen de la requête DNS:

![](./IVDNS.png)

L'adresse IP du serveur DNS est donc  216.58.214.174.

