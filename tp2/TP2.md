# TP2 : On va router des trucs



## I. ARP

ping de `node1` vers `node2` : 
```
[samedi@node1 ~]$ ping 10.2.1.12
PING 10.2.1.12 (10.2.1.12) 56(84) bytes of data.
64 bytes from 10.2.1.12: icmp_seq=1 ttl=64 time=0.435 ms
64 bytes from 10.2.1.12: icmp_seq=2 ttl=64 time=0.279 ms
^C
--- 10.2.1.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1050ms
rtt min/avg/max/mdev = 0.279/0.357/0.435/0.078 ms
```

table ARP de node1 trouvée à l'aide `arp` : 
```
[samedi@node1 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.1                 ether   0a:00:27:00:00:50   C                     enp0s8
10.2.1.12                ether   08:00:27:0e:e7:83   C                     enp0s8
```
table ARP de node2 :
```
[samedi@node2 ~]$ arp
Address                  HWtype  HWaddress           Flags Mask            Iface
10.2.1.1                 ether   0a:00:27:00:00:50   C                     enp0s8
10.2.1.11                ether   08:00:27:2f:5a:6a   C                     enp0s8
```

Adresse MAC de la carte réseau de node2 : `08:00:27:0e:e7:83`

Adresse MAC de la carte réseau de node1 :
`08:00:27:2f:5a:6a`

On peut vérifier que les adresses MAC correspondent à l'aide de `ip a` : 
 - pour node1 : 
```
[samedi@node1 ~]$ ip a
<...>
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:2f:5a:6a brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.11/24 brd 10.2.1.255 scope global 
<...>
```

 - pour node2 : 
```
[samedi@node2 ~]$ ip a
<...>
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0e:e7:83 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.12/24 brd 10.2.1.255 scope global <...>
```

### 2. Analyse de trames

On commence par vider les tables ARP avec `ip neigh flush all`: 
```
[samedi@node2 ~]$ sudo ip neigh flush all
[sudo] password for samedi:
```

On lance l'écoute de port avec `tcpdump` : 
```

```
1arp.png

On obtient le tableau suivant : 
| ordre | type trame  | source                   | destination                |
|-------|-------------|--------------------------|----------------------------|
| 1     | Requête ARP | `node1` `08:00:27:2f:5a:6a` | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Réponse ARP | `node2` `08:00:27:0e:e7:83` | `node1` `08:00:27:2f:5a:6a`   |



## II. Routage

### 1. Mise en place du routage

Pour mettre en place le routage il faut modifier la configuartion du firewall, il faut donc utiliser `firewall-cmd`: 
```
[samedi@router ~]$ sudo firewall-cmd --add-masquerade --zone=public
success
[samedi@router ~]$ sudo firewall-cmd --add-masquerade --zone=public --permanent
success
```

On peut maintenant voir que le routage est activé via `sudo firewall-cmd --list-all`: 
```
[samedi@router ~]$ sudo firewall-cmd --list-all
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s3 enp0s8
  sources:
  services: cockpit dhcpv6-client ssh
  ports:
  protocols:
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

On peut ajouter une route statique avec la commande `ip route add`:
```
[samedi@marcel ~]$  sudo ip route add 10.2.1.0/24 via 10.2.2.254 dev enp0s8
```
On rend ensuite la règle permanente en écrivant la règle dans le fichier `route-<NOM DE L'INTERFACE>` dans `/etc/sysconfig/network/scripts`.

Une fois cela fait sur les 2 machines, elles peuvent se `ping` :
```
[samedi@node1 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=62 time=2.93 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=62 time=2.22 ms
64 bytes from 10.2.2.12: icmp_seq=3 ttl=62 time=1.98 ms
^C
--- 10.2.2.12 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2003ms
rtt min/avg/max/mdev = 1.980/2.375/2.926/0.401 m
```

### 2. Analyse de trames

Après avoir regardé les tables ARP je pense que mes échanges se passent de cette façon : 
 - node1 envoie directement une requête à sa passerelle car il se fie à la route statique
 - le routeur transmet la requête à l'entièreté des appareils du réseau 10.2.2.0/24 vu que sa table ARP a été vidée
 - Marcel voie la requête qui lui est adressée et envoie une réponse via sa passerelle qui cette fois va directement à node1 après avoir transité par le routeur car on connait l'adresse MAC de node1.

| ordre | type trame  | IP source | MAC source                | IP destination | MAC destination            |
|-------|-------------|-----------|---------------------------|----------------|----------------------------|
| 1     | Requête ARP | x         | `node1` `08:00:27:2f:5a:6a`  | x              | Broadcast `FF:FF:FF:FF:FF` |
| 2     | Requête ARP | x         | `router` `08:00:27:9d:18:22` | x              | Broadcast `FF:FF:FF:FF:FF` |
| 3     | Réponse ARP | x         | `marcel` `08:00:27:7f:67:c6` | x              | `router` ` 08:00:27:9d:18:22`|
| 4     | Réponse ARP | x         | `router` ` 08:00:27:f9:1f:4d`| x              | `node1` `08:00:27:2f:5a:6a` |
| 5     | Ping        | `node1` `10.2.1.11`| `node1` `08:00:27:2f:5a:6a`|`marcel` `10.2.2.12`|`router` `08:00:27:f9:1f:4d` |
| 6     | Pong        | `marcel` `10.2.2.12` | `marcel` `08:00:27:7f:67:c6`   | `node1` `10.2.1.11`   |`router``08:00:27:9d:18:22` |

ajouter lien screen trames


### 3. Accès internet

ajout d'une route par défaut via `ip route add default` : 
```
[samedi@node1 ~]$ sudo ip route add default via 10.2.1.254 dev enp0s8
[sudo] password for samedi:
```
Pour rendre le changement permanent on ajoute la ligne `GATEWAY=10.2.1.254` dans `/etc/sysconfig/network`.

ping vers `8.8.8.8`:
```
[samedi@node1 sysconfig]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=27.1 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=114 time=24.8 ms
64 bytes from 8.8.8.8: icmp_seq=3 ttl=114 time=22.1 ms
^C
--- 8.8.8.8 ping statistics ---
3 packets transmitted, 3 received, 0% packet loss, time 2004ms
rtt min/avg/max/mdev = 22.082/24.648/27.078/2.045 ms
```

On ajoute un DNS en modifiant le fichier `/etc/sysconfig/network-scripts/ifcfg-enp0s8` (déjà fait précédement).

dig vers `google.com` : 
```
[samedi@node1 ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 11151
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

```

Ping vers `youtube.com`:
```
[samedi@node1 ~]$ ping youtube.com
PING youtube.com (142.250.74.238) 56(84) bytes of data.
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=1 ttl=113 time=22.5 ms
64 bytes from par10s40-in-f14.1e100.net (142.250.74.238): icmp_seq=2 ttl=113 time=31.6 ms
<...>
--- youtube.com ping statistics ---
6 packets transmitted, 6 received, 0% packet loss, time 5009ms
rtt min/avg/max/mdev = 22.492/26.982/31.564/2.834 ms
```

Analyse de trames : 

| ordre | type trame | IP source           | MAC source               | IP destination | MAC destination |
|-------|------------|---------------------|--------------------------|----------------|-----------------|
| 1     | ping       | `node1` `10.2.1.11` | `node1` `08:00:27:2f:5a:6a`| `8.8.8.8`    |`router` ` 08:00:27:f9:1f:4d`| 
| 2     | pong       | `8.8.8.8`           |`router` ` 08:00:27:f9:1f:4d`|`node1` `10.2.1.11`|`node1` `08:00:27:2f:5a:6a` |



## III. DHCP

### 1. Mise en place du serveur DHCP

On commence par installer le package `dhcp-server` : 

```
[samedi@node1 ~]$ sudo dnf install -y dhcp-server
Last metadata expiration check: 7:04:06 ago on Thu 23 Sep 2021 02:57:33 PM CEST.
Dependencies resolved.
<...>
```

Une fois cela fait on part éditer son fichier de config qui se trouve à `/etc/dhcp/dhcpd.conf` : 
 voici mon fichier de config : 
```
option domain-name     "srv.world";
# specify DNS server's hostname or IP address
option domain-name-servers     dlp.srv.world;
# default lease time
default-lease-time 600;
# max lease time
max-lease-time 7200;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.15 10.2.1.253;
    # specify broadcast address
    option broadcast-address 10.2.1.255;
    # specify gateway
    option routers 10.2.1.254;
}
```
Il faut ensuite lancer le dhcp et l'autoriser sur le firewall coté serveur : 

```
[samedi@node1 ~]$ sudo firewall-cmd --add-service=dhcp
[sudo] password for samedi:
success
[samedi@node1 ~]$ sudo firewall-cmd --runtime-to-permanent
success
[samedi@node1 ~]$ sudo systemctl enable --now dhcpd
Created symlink /etc/systemd/system/multi-user.target.wants/dhcpd.service → /usr/lib/systemd/system/dhcpd.service.
[samedi@node1 ~]$
```

Pour finir on repasse configuration de la carte réseau de `node2` en DHCP et on force la récupération d'une IP via la commande `dhclient`.


Amélioration de la configuration du DHCP : 

nouvelle configuration : 
```
# DHCP Server Configuration file.
#   see /usr/share/doc/dhcp-server/dhcpd.conf.example
#   see dhcpd.conf(5) man page
#
# specify DNS server's hostname or IP address
option domain-name-servers     8.8.8.8;
# default lease time
default-lease-time 60000;
# max lease time
max-lease-time 72000;
# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.2.1.0 netmask 255.255.255.0 {
    # specify the range of lease IP address
    range dynamic-bootp 10.2.1.15 10.2.1.253;
    # specify broadcast address
    option broadcast-address 10.2.1.255;
    # specify gateway
    option routers 10.2.1.254;
}
```
 vérification de la présence d'une ip avec `ip a`: 
```
[samedi@node2 ~]$ ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host
       valid_lft forever preferred_lft forever
2: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:0e:e7:83 brd ff:ff:ff:ff:ff:ff
    inet 10.2.1.17/24 brd 10.2.1.255 scope global dynamic noprefixroute enp0s8
       valid_lft 57880sec preferred_lft 57880sec
    inet6 fe80::a00:27ff:fe0e:e783/64 scope link
       valid_lft forever preferred_lft forever
```
 `ping` de la passerelle : 
```
[samedi@node2 ~]$ ping 10.2.1.254
PING 10.2.1.254 (10.2.1.254) 56(84) bytes of data.
64 bytes from 10.2.1.254: icmp_seq=1 ttl=64 time=0.487 ms
64 bytes from 10.2.1.254: icmp_seq=2 ttl=64 time=0.419 ms
^C
--- 10.2.1.254 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1053ms
rtt min/avg/max/mdev = 0.419/0.453/0.487/0.034 ms
[samedi@node2 ~]$
```
vérifivation de la présence d'une route avec `ip r s`: 
```
[samedi@node2 ~]$ ip r s
default via 10.2.1.254 dev enp0s8
default via 10.2.1.254 dev enp0s8 proto dhcp metric 100
10.2.1.0/24 dev enp0s8 proto kernel scope link src 10.2.1.17 metric 100
[samedi@node2 ~]$
```

`ping` de vers Marcel : 
```
[samedi@node2 ~]$ ping 10.2.2.12
PING 10.2.2.12 (10.2.2.12) 56(84) bytes of data.
64 bytes from 10.2.2.12: icmp_seq=1 ttl=63 time=0.813 ms
64 bytes from 10.2.2.12: icmp_seq=2 ttl=63 time=0.579 ms
^C
--- 10.2.2.12 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1004ms
rtt min/avg/max/mdev = 0.579/0.696/0.813/0.117 ms
```

`dig` vers `youtube.com`: 
```
[samedi@node2 ~]$ dig youtube.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> youtube.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 41183
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1

<...>

```

`ping` vers `youtube.com`: 
```
[samedi@node2 ~]$ ping youtube.com
PING youtube.com (216.58.213.174) 56(84) bytes of data.
64 bytes from par21s04-in-f14.1e100.net (216.58.213.174): icmp_seq=1 ttl=114 time=21.3 ms
64 bytes from par21s04-in-f14.1e100.net (216.58.213.174): icmp_seq=2 ttl=114 time=19.8 ms
64 bytes from par21s04-in-f14.1e100.net (216.58.213.174): icmp_seq=3 ttl=114 time=19.9 ms
64 bytes from par21s04-in-f14.1e100.net (216.58.213.174): icmp_seq=4 ttl=114 time=21.7 ms
^C
--- youtube.com ping statistics ---
4 packets transmitted, 4 received, 0% packet loss, time 3006ms
rtt min/avg/max/mdev = 19.771/20.660/21.684/0.846 ms
```


### 2. Analyse de trames


On vide les tables ARP à l'aide de `sudo ip neigh flush all`: 
```
[samedi@node2 ~]$ sudo ip neigh flush all
[sudo] password for samedi:
```

On renouvelle le bail DHCP à l'aide de ` sudo dhclient -r enp0s8` : 
```
[samedi@node2 ~]$ sudo dhclient -r enp0s8
[sudo] password for samedi:
Killed old client process

```

Mise en évidence de l'échange DHCP DORA depuis `node1`: 

![](./DHCP_DORA.png)


tableau récapitulatif : 
| ordre | type trame  | source                      | destination                |
|-------|-------------|-----------------------------|----------------------------|
| 1     | ARP | `node1` `08:00:27:2f:5a:6a` | `node2` `08:00:27:0e:e7:83` |
| 2     | ARP | `node2` `08:00:27:0e:e7:83` | `node1` `08:00:27:2f:5a:6a`   
| 3     | DHCP Discover | `node2` `08:00:27:0e:e7:83` | Broadcast `FF:FF:FF:FF:FF` |
| 4     | DHCP Offer | `node1` `08:00:27:2f:5a:6a` | `node2` `08:00:27:0e:e7:83`   
| 5     | DHCP Request | `node2` `08:00:27:0e:e7:83` | Broadcast `FF:FF:FF:FF:FF` |
| 6     | DHCP ACK | `node1` `08:00:27:2f:5a:6a` | `node2` `08:00:27:0e:e7:83` 
