# TP3 : Progressons vers le réseau d'infrastructure

# Sommaire

- [TP3 : Progressons vers le réseau d'infrastructure](#tp3--progressons-vers-le-réseau-dinfrastructure)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist](#checklist)
- [I. (mini)Architecture réseau](#i-miniarchitecture-réseau)
  - [1. Adressage](#1-adressage)
  - [2. Routeur](#2-routeur)
- [II. Services d'infra](#ii-services-dinfra)
  - [1. Serveur DHCP](#1-serveur-dhcp)
  - [2. Serveur DNS](#2-serveur-dns)
    - [A. Our own DNS server](#a-our-own-dns-server)
    - [B. SETUP copain](#b-setup-copain)
  - [3. Get deeper](#3-get-deeper)
    - [A. DNS forwarder](#a-dns-forwarder)
    - [B. On revient sur la conf du DHCP](#b-on-revient-sur-la-conf-du-dhcp)
- [Entracte](#entracte)
- [III. Services métier](#iii-services-métier)
  - [1. Serveur Web](#1-serveur-web)
  - [2. Partage de fichiers](#2-partage-de-fichiers)
    - [A. L'introduction wola](#a-lintroduction-wola)
    - [B. Le setup wola](#b-le-setup-wola)
- [IV. Un peu de théorie : TCP et UDP](#iv-un-peu-de-théorie--tcp-et-udp)
- [V. El final](#v-el-final)


# I. (mini)Architecture réseau


## 2. Routeur

`ip a` pour voir que le routeur possède une ip dans les 3 réseaux : 
```
[samedi@router ~]$ ip a
<...>
3: enp0s8: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:dd:d1:b8 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.62/26 brd 10.3.1.63 scope global noprefixroute enp0s8
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:fedd:d1b8/64 scope link
       valid_lft forever preferred_lft forever
4: enp0s9: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:aa:b3:14 brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.254/25 brd 10.3.1.255 scope global noprefixroute enp0s9
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feaa:b314/64 scope link
       valid_lft forever preferred_lft forever
5: enp0s10: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 08:00:27:b4:f0:4e brd ff:ff:ff:ff:ff:ff
    inet 10.3.1.78/28 brd 10.3.1.79 scope global noprefixroute enp0s10
       valid_lft forever preferred_lft forever
    inet6 fe80::a00:27ff:feb4:f04e/64 scope link
       valid_lft forever preferred_lft forever
```

`hostname` pour voir le nom de la machine : 
```
[samedi@router ~]$ hostname
router.tp3
```

`ping 8.8.8.8` pour vérifier l'accès internet : 
```
[samedi@router ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=115 time=25.4 ms
64 bytes from 8.8.8.8: icmp_seq=2 ttl=115 time=20.8 ms
^C
--- 8.8.8.8 ping statistics ---
2 packets transmitted, 2 received, 0% packet loss, time 1002ms
rtt min/avg/max/mdev = 20.754/23.094/25.435/2.345 ms
```

`dig youtube.com` pour vérifier la résolution de nom : 
```
[samedi@router ~]$ dig youtube.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> youtube.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 44802
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
<...>
```

`firewall-cmd --list-all` pour vérifier que le routage est déjà activé (pas besoin de l'activer car `router.tp3` est un clone de celui du tp2) : 
```
[samedi@router ~]$ sudo !!
sudo firewall-cmd --list-all
[sudo] password for samedi:
public (active)
  target: default
  icmp-block-inversion: no
  interfaces: enp0s10 enp0s3 enp0s8 enp0s9
  sources:
  services: cockpit dhcpv6-client ssh
  ports: 8888/tcp
  protocols:
  masquerade: yes
  forward-ports:
  source-ports:
  icmp-blocks:
  rich rules:
```

# II. Services d'infra

## 1. Serveur DHCP

`hostname` pour voir le nom de la machine : 
```
[samedi@dhcp etc]$ hostname
dhcp.client1.tp3
```

Pour le reste il suffit de regarder le fichier de config : 
```
lien vers le fichier de conf
```

Configuration de Marcel : 
 
`hostname` pour voir le nom de la machine : 
```
[samedi@marcel ~]$ hostname
marcel.client1.tp3
```

en faisant en `cat` sur `/var/lib/dhcpd/dhcpd.leases` dans `dhcp.client1.tp3` on peut voir que `Marcel` a bien reçu des informations du serveur DHCP : 
```
[samedi@dhcp etc]$ sudo cat /var/lib/dhcpd/dhcpd.leases
# The format of this file is documented in the dhcpd.leases(5) manual page.
# This lease file was written by isc-dhcp-4.3.6

# authoring-byte-order entry is generated, DO NOT DELETE
authoring-byte-order little-endian;

server-duid "\000\001\000\001(\346\345\332\010\000'\264\000_";

lease 10.3.1.5 {
  starts 3 2021/09/29 08:53:37;
  ends 3 2021/09/29 20:53:37;
  cltt 3 2021/09/29 08:53:37;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:73:0d:8a;
  uid "\001\010\000's\015\212";
  client-hostname "marcel";
}
```

`ping` pour tester l'accès internet : 
```
[samedi@marcel ~]$ ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=1 ttl=114 time=26.9 ms
^C
--- 8.8.8.8 ping statistics ---
1 packets transmitted, 1 received, 0% packet loss, time 0ms
rtt min/avg/max/mdev = 26.940/26.940/26.940/0.000 ms
```

`dig` pour tester la résolution de nom : 
```
[samedi@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 42075
;; flags: qr rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 0, ADDITIONAL: 1
```

`traceroute` pour vérifier que `marcel` sort bien de son réseau en passant par le routeur: 
```
[samedi@marcel ~]$ traceroute google.com
traceroute to google.com (142.250.201.174), 30 hops max, 60 byte packets
 1  _gateway (10.3.1.62)  1.671 ms  1.633 ms  1.616 ms
 <...>
```

![Configuration du DHCP](./files/dhcpd.conf)

## 2. Serveur DNS

### B. SETUP copain

installation du DNS : 
```
[samedi@dns1 ~]$ sudo dnf install -y bind bind-utils
```

On autorise le service dans le firewall : 
```
[samedi@dns1 var]$ sudo firewall-cmd --add-service=dns --permanent
success
[samedi@dns1 var]$ sudo firewall-cmd --reload
success
```

On donne les droits aux fichiers de configurations : 
```
[samedi@dns1 var]$ [samedi@dns1 var]$ sudo nano /var/named/server1.tp3.forward
[sudo] password for samedi:
[samedi@dns1 var]$ [samedi@dns1 var]$ sudo chown root:named /var/named/server1.tp3.forward
[sudo] password for samedi:
[samedi@dns1 var]$ sudo chmod 644 /var/named/server1.tp3.forward
```


On crée les fichiers de zone `server1.tp3.forward` et `server1.tp3.forward` 



Phase de test depuis `marcel.client1.tp3` : 

```
[samedi@marcel ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
search client1.tp3
nameserver 10.3.1.130
```

`dig` vers `google.com` : 

```
[samedi@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 44263
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: ef127e00281a14917a2c92e76155e3d430a4eb2cd8df5e2a (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; Query time: 1464 msec
;; SERVER: 10.3.1.130#53(10.3.1.130)
;; WHEN: Thu Sep 30 18:58:53 CEST 2021
;; MSG SIZE  rcvd: 67
```

`dig` pour test la zone forward : 

```
[samedi@marcel ~]$ dig dns1.server1.tp3

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> dns1.server1.tp3
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: NOERROR, id: 45177
;; flags: qr aa rd ra; QUERY: 1, ANSWER: 1, AUTHORITY: 1, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: a594829f73d945bc0470d61e6155e3eb61cc817ee280fcfb (good)
;; QUESTION SECTION:
;dns1.server1.tp3.              IN      A

;; ANSWER SECTION:
dns1.server1.tp3.       86400   IN      A       10.3.1.130

;; AUTHORITY SECTION:
server1.tp3.            86400   IN      NS      dns1.server1.tp3.

;; Query time: 2 msec
;; SERVER: 10.3.1.130#53(10.3.1.130)
;; WHEN: Thu Sep 30 18:59:16 CEST 2021
;; MSG SIZE  rcvd: 103
```

Mise en évidence du server dns utilisé pour réaliser les `dig` : 

```
[samedi@marcel ~]$ dig dns1.server1.tp3 | grep SERVER:
;; SERVER: 10.3.1.130#53(10.3.1.130)
[samedi@marcel ~]$ dig google.com | grep SERVER:
;; SERVER: 10.3.1.130#53(10.3.1.130)
```

## 3. Get deeper 

### A. DNS forwarder

Déjà fait en amont : 

`dig` vers `google.com` : 

```
[samedi@marcel ~]$ dig google.com

; <<>> DiG 9.11.26-RedHat-9.11.26-4.el8_4 <<>> google.com
;; global options: +cmd
;; Got answer:
;; ->>HEADER<<- opcode: QUERY, status: SERVFAIL, id: 44263
;; flags: qr rd ra; QUERY: 1, ANSWER: 0, AUTHORITY: 0, ADDITIONAL: 1

;; OPT PSEUDOSECTION:
; EDNS: version: 0, flags:; udp: 1232
; COOKIE: ef127e00281a14917a2c92e76155e3d430a4eb2cd8df5e2a (good)
;; QUESTION SECTION:
;google.com.                    IN      A

;; Query time: 1464 msec
;; SERVER: 10.3.1.130#53(10.3.1.130)
;; WHEN: Thu Sep 30 18:58:53 CEST 2021
;; MSG SIZE  rcvd: 67
```


### B. On revient sur la conf du DHCP 

changements dans la configuration du DHCP : 

```
[samedi@dhcp ~]$ sudo cat /etc/dhcp/dhcpd.conf | grep  domain
option domain-name-servers     10.3.1.130;
```

Preuve que Johnny a récupéré ses infos via le serveur DHCP : 
```
[samedi@dhcp ~]$ sudo cat /var/lib/dhcpd/dhcpd.leases
# The format of this file is documented in the dhcpd.leases(5) manual page.
# This lease file was written by isc-dhcp-4.3.6

# authoring-byte-order entry is generated, DO NOT DELETE
authoring-byte-order little-endian;
<...>
lease 10.3.1.7 {
  starts 4 2021/09/30 11:20:34;
  ends 4 2021/09/30 23:20:34;
  cltt 4 2021/09/30 11:20:34;
  binding state active;
  next binding state free;
  rewind binding state free;
  hardware ethernet 08:00:27:48:a3:ea;
  uid "\001\010\000'H\243\352";
  client-hostname "johnny";
}
<...>
```


# III. Services métier


Installation du serveur web : 

```
[samedi@web1 ~]$ sudo dnf install nginx
Rocky Linux 8 - AppStream                                                               4.5 MB/s | 9.1 MB     00:02
Rocky Linux 8 - BaseOS
<...>
```

Modification de la cinfiguration du firewall :

```
[samedi@web1 ~]$ sudo firewall-cmd --permanent --zone=public --add-service=http
success
[samedi@web1 ~]$ sudo firewall-cmd --permanent --zone=public --add-service=https
success
```

Test depuis `Marcel.client1.tp3` : 

```
[samedi@marcel ~]$ curl web1.server2.tp3 | grep nginx
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100  3429  100  3429    0     0   558k      0 --:--:-- --:--:-- --:--:--  558k
    <h1>Welcome to <strong>nginx</strong> on Rocky Linux!</h1>
        <strong>nginx</strong> HTTP server after it has been installed. If you
            with <strong>nginx</strong> on Rocky Linux. It is located in
            <tt>/usr/share/nginx/html</tt>.
            <strong>nginx</strong>
            <tt>/etc/nginx/nginx.conf</tt>.
        <a href="http://nginx.net/"
            src="nginx-logo.png"
            alt="[ Powered by nginx ]"
```

## 2. Partage de fichiers

### B. Le setup wola


Installation du paquet : 

```
[samedi@web1 ~]$  sudo dnf install -y nfs-utils
Rocky Linux 8 - AppStream
<...>
```

Démarrage du service : 

```
[samedi@web1 ~]$ sudo systemctl start nfs-server.service
[sudo] password for samedi:
[samedi@web1 ~]$ sudo systemctl enable nfs-server.service
Created symlink /etc/systemd/system/multi-user.target.wants/nfs-server.service → /usr/lib/systemd/system/nfs-server.service.
```

Mise en place du fichier à partager : 

```
[samedi@nfs1 ~]$ sudo mkdir -p /srv/nfs_share
[sudo] password for samedi:

[samedi@nfs1 srv]$ cat /etc/exports
/srv/nfs_share/  10.3.1.67(rw,sync)

[samedi@nfs1 srv]$ sudo exportfs -arv
exporting 10.3.1.67:/srv/nfs_share
```

Modification du firewall : 

```
[samedi@nfs1 srv]$ sudo firewall-cmd --permanent --add-service=nfs
[sudo] password for samedi:
success
[samedi@nfs1 srv]$  sudo firewall-cmd --permanent --add-service=rpc-bind
success
[samedi@nfs1 srv]$ sudo firewall-cmd --permanent --add-service=mountd
success
[samedi@nfs1 srv]$ sudo firewall-cmd --reload
success
```

Configuration du client : 

```
[samedi@web1 ~]$ sudo dnf install -y nfs-utils nfs4-acl-tools
[sudo] password for samedi:
Rocky Linux 8 - AppStream
```

```
[samedi@web1 ~]$ showmount -e 10.3.1.67
Export list for 10.3.1.67:
/srv/nfs_share 10.3.1.67
```

```
[samedi@web1 ~]$ sudo mkdir -p /srv/nfs
[sudo] password for samedi:
[samedi@web1 ~]$ sudo mount -t nfs 10.3.1.67:/srv/nfs_share /srv/nfs

[samedi@web1 ~]$ cat /etc/fstab | grep nfs
10.3.1.67:/srv/nfs_share     /srv/nfs  nfs     defaults 0 0
```

Test : 






# IV. Un peu de théorie : TCP et UDP


- SSH : TCP
- HTTP : TCP
- DNS : UDP
- NFS : TCP


![Capture du SSH](./files/tp3_ssh.pcap)
![Capture du HTTP](./files/tp3_http.pcap)
![Capture du DNS](./files/tp3_dns.pcap)
![Capture du 3-way](./files/tp3_3way.pcap)

# V. El final


| Nom du réseau | Adresse du réseau | Masque        | Nombre de clients possibles | Adresse passerelle | [Adresse broadcast] |
|---------------|-------------------|---------------|-----------------------------|--------------------|----------------------------------------------------------------------------------------------|
| `client1`     | `10.3.1.0`        | `255.255.255.192` | 62                           | `10.3.1.62`         | `10.3.1.63`                                                                                   |
| `server1`     | `10.3.1.128`        | `255.255.255.128` | 126                        | `10.3.1.254`         | `10.3.1.255`                                                                                   |
| `server2`     | `10.3.1.64`        | `255.255.255.240` | 14                          | `10.3.1.79`         | `10.3.1.80`                                                                                   |


| Nom machine    | Adresse IP `client1` | Adresse IP `server1` | Adresse IP `server2` | Adresse de passerelle |
|----------------|----------------------|----------------------|----------------------|-----------------------|
| `router.tp3`   | `10.3.1.62/26`       | `10.3.1.254/25`      | `10.3.1.79/28`       | Carte NAT             |
| `dhcp.client1.tp3`    | `10.3.1.2/26` | x                    | x                    |  `10.3.1.62/26`       |
| `marcel.client1.tp3`  | `10.3.1.5/26` | x                    | x                    |  `10.3.1.62/26`       |
| `johnny.client1.tp3`  | `10.3.1.6/26` | x                    | x                    |  `10.3.1.63/26`       |
| `dns1.server1.tp3`    |`10.3.1.254/25`| x                    | x                    |  `10.3.1.254/25`      |
| `web1.server2.tp3`    |`10.3.1.66/28` | x                    | x                    |  `10.3.1.79/29`       |
| `nfs1.server2.tp3`    |`10.3.1.67/28` | x                    | x                    |  `10.3.1.79/29`       |

Schéma final : 

![Schéma](./files/schéma.drawio)
   

Fichier de configuration du DNS : 

![Fichier principal](./files/named.conf)
![zone clien1](./files/client1.tp3.forward)
![zone server1](./files/server1.tp3.forward)
![zone server2](./files/server2.tp3.forward)
