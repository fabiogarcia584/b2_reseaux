# TP4 : Vers un réseau d'entreprise

On va utiliser GNS3 dans ce TP pour se rapprocher d'un cas réel. On va focus sur l'aspect routing/switching, avec du matériel Cisco. On va aussi mettre en place des VLANs.

# Sommaire

- [TP4 : Vers un réseau d'entreprise](#tp4--vers-un-réseau-dentreprise)
- [Sommaire](#sommaire)
- [0. Prérequis](#0-prérequis)
  - [Checklist VM Linux](#checklist-vm-linux)
- [I. Dumb switch](#i-dumb-switch)
  - [1. Topologie 1](#1-topologie-1)
  - [2. Adressage topologie 1](#2-adressage-topologie-1)
  - [3. Setup topologie 1](#3-setup-topologie-1)
- [II. VLAN](#ii-vlan)
  - [1. Topologie 2](#1-topologie-2)
  - [2. Adressage topologie 2](#2-adressage-topologie-2)
    - [3. Setup topologie 2](#3-setup-topologie-2)
- [III. Routing](#iii-routing)
  - [1. Topologie 3](#1-topologie-3)
  - [2. Adressage topologie 3](#2-adressage-topologie-3)
  - [3. Setup topologie 3](#3-setup-topologie-3)
- [IV. NAT](#iv-nat)
  - [1. Topologie 4](#1-topologie-4)
  - [2. Adressage topologie 4](#2-adressage-topologie-4)
  - [3. Setup topologie 4](#3-setup-topologie-4)


# I. Dumb switch


## 3. Setup topologie 1

🌞 **Commençons simple**

```bash=
PC1> ip 10.1.1.1/24
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0

PC1> show ip all

NAME   IP/MASK              GATEWAY           MAC                DNS
PC1    10.1.1.1/24          255.255.255.0     00:50:79:66:68:00

PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=5.239 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=7.047 ms

```




```bash=
PC2> ip 10.1.1.2/24
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0

PC2> show ip all

NAME   IP/MASK              GATEWAY           MAC                DNS
PC2    10.1.1.2/24          255.255.255.0     00:50:79:66:68:01

PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=2.015 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=2.473 ms

```

# II. VLAN

### 3. Setup topologie 2

🌞 **Adressage**

```bash=
PC3> ip 10.1.1.3/24
Checking for duplicate address...
PC3 : 10.1.1.3 255.255.255.0

PC3> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=13.058 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=14.128 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=7.359 ms
^C
PC3> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=9.048 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=14.262 ms
^C

```

🌞 **Configuration des VLANs**

```bash=
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#vlan 01
Switch(config-vlan)#exit
Switch(config)#vlan 10
Switch(config-vlan)#exit
Switch(config)#vlan 20
Switch(config-vlan)#exit
Switch(config)#interface GigabitEthernet 0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vl
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet 0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 10
Switch(config-if)#exit
Switch(config)#interface GigabitEthernet 0/2
Switch(config-if)#
Switch(config-if)#switchport mode access
Switch(config-if)#switchport access vlan 20
Switch(config-if)#exit
Switch#wr
Building configuration...
Compressed configuration from 3655 bytes to 1635 bytes[OK]
Switch#
```

🌞 **Vérif**


```bash=

PC2> ping 10.1.1.1

84 bytes from 10.1.1.1 icmp_seq=1 ttl=64 time=7.291 ms
84 bytes from 10.1.1.1 icmp_seq=2 ttl=64 time=5.009 ms
84 bytes from 10.1.1.1 icmp_seq=3 ttl=64 time=6.192 ms
84 bytes from 10.1.1.1 icmp_seq=4 ttl=64 time=4.807 ms
84 bytes from 10.1.1.1 icmp_seq=5 ttl=64 time=7.496 ms

PC2> ping 10.1.1.3

host (10.1.1.3) not reachable



PC3> ping 10.1.1.1

host (10.1.1.1) not reachable

PC3> ping 10.1.1.2

host (10.1.1.2) not reachable



PC1> ping 10.1.1.2

84 bytes from 10.1.1.2 icmp_seq=1 ttl=64 time=21.045 ms
84 bytes from 10.1.1.2 icmp_seq=2 ttl=64 time=14.451 ms
^C
PC1> ping 10.1.1.3

host (10.1.1.3) not reachable


```


# III. Routing


🌞 **Adressage**

Client 1 et Client 2 ont déjà été configurés plus tôt.

```bash=
PC3> ip 10.2.2.1/24
Checking for duplicate address...
PC3 : 10.2.2.1 255.255.255.0

```
Pour la VM : 
```bash=
[samedi@web ~]$ ip a | grep enp0s3
2: enp0s3: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    inet 10.3.3.1/24 brd 10.3.3.255 scope global noprefixroute enp0s3
```

🌞 **Configuration des VLANs**

```bash=

Switch>enable
Switch#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
Switch(config)#vlan 11
Switch(config-vlan)#name clients
Switch(config-vlan)#exit
Switch(config)#vlan 12
Switch(config-vlan)#name admins
Switch(config-vlan)#exit
Switch(config)#vlan 13
Switch(config-vlan)#name servers
Switch(config-vlan)#ex
Switch(config-vlan)#exit
Switch(config)#interface Gi0/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport acces vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/1
Switch(config-if)#switchport mode access
Switch(config-if)#switchport acces vlan 11
Switch(config-if)#exit
Switch(config)#interface Gi0/2
Switch(config-if)#switchport mode access
Switch(config-if)#switchport acces vlan 12
Switch(config-if)#exit
Switch(config)#interface Gi1/0
Switch(config-if)#switchport mode access
Switch(config-if)#switchport acces vlan 13
Switch(config-if)#exit
Switch(config)#interface Gi0/3
Switch(config-if)#switchport trunk encapsulation dot1q
Switch(config-if)#switchport mode trunk
Switch(config-if)#switchport trunk allowed vlan add 11,12,13
Switch(config-if)#exit
Switch(config)#exit
Switch#wr
Building configuration...
Compressed configuration from 3774 bytes to 1696 bytes[OK]
*Oct 21 12:09:06.229: %GRUB-5-CONFIG_WRITING: GRUB configuration is being updated on disk. Please wait...
*Oct 21 12:09:06.930: %GRUB-5-CONFIG_WRITTEN: GRUB configuration was written to disk successfully.
Switch#

```

🌞 **Config du *routeur***

```bash=
R1#enable
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#inter
R1(config)#interface f0/0
R1(config-if)#no shut
R1(config-if)#
*Mar  1 01:11:18.727: %LINK-3-UPDOWN: Interface FastEthernet0/0, changed state to up
*Mar  1 01:11:19.727: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet0/0, changed state to up
R1(config-if)#exit
R1(config)#interface f0/0.11
R1(config-subif)#encapsulation dot1Q 11
R1(config-subif)#ip addr 10.1.1.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface f0/0.12
R1(config-subif)#encapsulation dot1Q 12
R1(config-subif)#ip addr 10.2.2.254 255.255.255.0
R1(config-subif)#exit
R1(config)#interface f0/0.13
R1(config-subif)#encapsulation dot1Q 13
R1(config-subif)#ip addr 10.3.3.254 255.255.255.0
R1(config-subif)#exit
R1(config)#exit
R1#wr
Building configuration...
[OK]
R1#
```

**Vérif**

- tout le monde doit pouvoir ping le routeur sur l'IP qui est dans son réseau

```bash=
PC1> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=19.846 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=14.952 ms

PC2> ping 10.1.1.254

84 bytes from 10.1.1.254 icmp_seq=1 ttl=255 time=20.046 ms
84 bytes from 10.1.1.254 icmp_seq=2 ttl=255 time=5.835 ms

adm1> ping 10.2.2.254

84 bytes from 10.2.2.254 icmp_seq=1 ttl=255 time=22.941 ms
84 bytes from 10.2.2.254 icmp_seq=2 ttl=255 time=19.092 ms

[samedi@web ~]$ ping 10.3.3.254
PING 10.3.3.254 (10.3.3.254) 56(84) bytes of data.
64 bytes from 10.3.3.254: icmp_seq=1 ttl=255 time=39.7 ms
64 bytes from 10.3.3.254: icmp_seq=2 ttl=255 time=16.4 ms

```

- en ajoutant une route vers les réseaux, ils peuvent se ping entre eux


    ajout des routes : 
```bash=
PC1> ip 10.1.1.1/24 10.1.1.254
Checking for duplicate address...
PC1 : 10.1.1.1 255.255.255.0 gateway 10.1.1.254

PC2> ip 10.1.1.2/24 10.1.1.254
Checking for duplicate address...
PC2 : 10.1.1.2 255.255.255.0 gateway 10.1.1.254

adm1> ip 10.2.2.1/24 10.2.2.254
Checking for duplicate address...
adm1 : 10.2.2.1 255.255.255.0 gateway 10.2.2.254

[samedi@web ~]$ cat /etc/sysconfig/network
# Created by anaconda
GATEWAY=10.3.3.254

[samedi@web ~]$ cat /etc/sysconfig/network-scripts/route-ens33
10.1.1.0/24 via 10.3.3.254 dev ens33
10.2.2.0/24 via 10.3.3.254 dev ens33
```

- testez des `ping` entre les réseaux

```bash=
PC2> ping 10.2.2.1

84 bytes from 10.2.2.1 icmp_seq=1 ttl=63 time=38.998 ms
84 bytes from 10.2.2.1 icmp_seq=2 ttl=63 time=40.209 ms

PC2> ping 10.3.3.1

84 bytes from 10.3.3.1 icmp_seq=1 ttl=63 time=28.624 ms
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=42.279 ms


```

# IV. NAT

## 3. Setup topologie 4

🌞 **Ajoutez le noeud Cloud à la topo**


Récupération d'une ip en DHCP : 

```bash=
R1#enable
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface f1/0
R1(config-if)#no shut
R1(config-if)#
*Mar  1 01:36:16.135: %LINK-3-UPDOWN: Interface FastEthernet1/0, changed state to up
*Mar  1 01:36:17.135: %LINEPROTO-5-UPDOWN: Line protocol on Interface FastEthernet1/0, changed state to up
R1(config-if)#ip address dhcp
R1(config-if)#exit
R1(config)#exit
R1#
*Mar  1 01:36:36.059: %DHCP-6-ADDRESS_ASSIGN: Interface FastEthernet1/0 assigned DHCP address 10.0.3.16, mask 255.255.255.0, hostname R1

R1#
*Mar  1 01:36:36.151: %SYS-5-CONFIG_I: Configured from console by console
R1#wr
Building configuration...
[OK]
R1#show ip int br
Interface                  IP-Address      OK? Method Status                Protocol
FastEthernet0/0            unassigned      YES unset  up                    up
FastEthernet0/0.11         10.1.1.254      YES manual up                    up
FastEthernet0/0.12         10.2.2.254      YES manual up                    up
FastEthernet0/0.13         10.3.3.254      YES manual up                    up
FastEthernet1/0            10.0.3.16       YES DHCP   up                    up
FastEthernet2/0            unassigned      YES unset  administratively down down
FastEthernet3/0            unassigned      YES unset  administratively down down
```

Test de `ping` vers `1.1.1.1` : 
```bash=
R1#ping 1.1.1.1

Type escape sequence to abort.
Sending 5, 100-byte ICMP Echos to 1.1.1.1, timeout is 2 seconds:
!!!!!
Success rate is 100 percent (5/5), round-trip min/avg/max = 12/32/56 ms
```

🌞 **Configurez le NAT**

```bash=
R1#enable
R1#conf t
Enter configuration commands, one per line.  End with CNTL/Z.
R1(config)#interface f0/0
R1(config-if)#ip nat inside

*Mar  1 01:41:18.823: %LINEPROTO-5-UPDOWN: Line protocol on Interface NVI0, changed state to up
R1(config-if)#exit
R1(config)#interface f1/0
R1(config-if)#ip nat outside
R1(config-if)#exit
R1(config)#access-list 1 permit any
R1(config)#ip nat inside source list 1 interface f1/0 overload
R1(config)#exit
R1#
*Mar  1 01:42:37.975: %SYS-5-CONFIG_I: Configured from console by console
R1#wr
Building configuration...
[OK]

```

🌞 **Test**

Les routes ont déjà été établies, ajout des DNS : 

```bash=
PC1> ip dns 1.1.1.1

PC2> ip dns 1.1.1.1

adm1> ip dns 1.1.1.1

[samedi@web ~]$ cat /etc/resolv.conf
# Generated by NetworkManager
nameserver 1.1.1.1

```

Test de `ping` vers `google.com` :

```bash=
PC2> ping google.com
google.com resolved to 172.217.19.238

84 bytes from 172.217.19.238 icmp_seq=1 ttl=113 time=57.740 ms
84 bytes from 172.217.19.238 icmp_seq=2 ttl=113 time=45.906 ms

```

# V. Add a building

## 3. Setup topologie 5

🌞  **Vous devez me rendre le `show running-config` de tous les équipements**

[config R1](config/R1.conf) 
[config SW1](config/SW1.conf) 
[config SW2](config/SW2.conf) 
[config SW3](config/SW3.conf)

🌞  **Mettre en place un serveur DHCP dans le nouveau bâtiment**

Configuration du serveur DHCP : 

```bash=
[samedi@dhcp ~]$ cat /etc/dhcp/dhcpd.conf

# this DHCP server to be declared valid
authoritative;
# specify network address and subnetmask
subnet 10.1.1.0 netmask 255.255.255.0 {
    option subnet-mask 255.255.255.0;
    # specify the range of lease IP address
    range dynamic-bootp 10.1.1.4 10.1.1.252;
    # specify broadcast address
    option broadcast-address 10.3.1.63;
    # specify gateway
    option routers 10.1.1.254;
    # specifiy dns
    option domain-name-servers 1.1.1.1;
}
```

🌞  **Vérification**


- un client récupère une IP en DHCP
```bash = 
PC3> ip dhcp
DDORA IP 10.1.1.4/24 GW 10.1.1.254

PC3> show ip

NAME        : PC3[1]
IP/MASK     : 10.1.1.4/24
GATEWAY     : 10.1.1.254
DNS         : 1.1.1.1
DHCP SERVER : 10.1.1.253
DHCP LEASE  : 43131, 43200/21600/37800
MAC         : 00:50:79:66:68:03
LPORT       : 20064
RHOST:PORT  : 127.0.0.1:20065
MTU         : 1500

```

- il peut ping le serveur Web

```bash=
PC3> ping 10.3.3.1

10.3.3.1 icmp_seq=1 timeout
84 bytes from 10.3.3.1 icmp_seq=2 ttl=63 time=38.663 ms
84 bytes from 10.3.3.1 icmp_seq=3 ttl=63 time=39.938 ms
84 bytes from 10.3.3.1 icmp_seq=4 ttl=63 time=85.831 ms
```

- il peut ping `8.8.8.8` 

```bash=
PC3> ping 8.8.8.8

8.8.8.8 icmp_seq=1 timeout
84 bytes from 8.8.8.8 icmp_seq=2 ttl=114 time=73.020 ms
84 bytes from 8.8.8.8 icmp_seq=3 ttl=114 time=48.358 ms
```

- il peut ping `google.com` 

``` bash=
PC3> ping google.com
google.com resolved to 216.58.204.110

84 bytes from 216.58.204.110 icmp_seq=1 ttl=113 time=59.677 ms
84 bytes from 216.58.204.110 icmp_seq=2 ttl=113 time=68.573 ms
```
